{-|
Module      : Constants

A bunch of constants used in the application
-}
module Constants where

    -- | Images supported types extenstions for file picker 
    imageFileExtensions :: [(String, [String])]
    imageFileExtensions = [("Image files",["*.bmp","*.jpg","*.gif","*.png"])
        ,("Portable Network Graphics (*.png)",["*.png"])
        ,("BMP files (*.bmp)",["*.bmp"])
        ,("JPG files (*.jpg)",["*.jpg"])
        ,("GIF files (*.gif)",["*.gif"])
        ]

    -- | Working directory path
    preprocessedFilesPath :: String
    preprocessedFilesPath = "./Preprocessing/"

    -- | Dimensions of canvas to draw on
    height, width :: Int
    height   = 600
    width    = 600