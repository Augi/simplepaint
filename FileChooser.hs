{-|
Module      : FileChooser

Functions responsible for setting up environment to start work on images preprocessing. 
These actions are: 
    choosing an image from a file system, 
    creating copy of the image,
    setting images paths in two variables.
-}
module FileChooser where 

    import Graphics.UI.WX hiding (Event)
    import Graphics.UI.WXCore
    import Graphics.Image as I
    import Data.List.Split
    import Constants

    -- | Event handler that opens file picker
    onOpen :: Frame a -> Var (Maybe FilePath) -> Var (Maybe FilePath) -> StatusField -> IO ()
    onOpen f vdefaultbitmap vbitmap status = do 
        mbfname <- fileOpenDialog f False True "Open image" imageFileExtensions "" ""
        case mbfname of
            Nothing    -> return ()
            Just fname -> copyImageRet fname >>= (\path -> setImagePath vdefaultbitmap vbitmap status fname path)

    -- | Stores path to chosen file and path to temporary file in two variables and displays file path in a status bar 
    setImagePath :: Var (Maybe FilePath) -> Var (Maybe FilePath) -> StatusField -> FilePath -> FilePath -> IO ()
    setImagePath vdefaultbitmap vbitmap status defaultpath modifiedpath = do
        set vbitmap [value := Just modifiedpath]
        set vdefaultbitmap [value := Just defaultpath]
        set status [text := defaultpath]

    -- | Copies an image to working directory and returns relative copy image path
    copyImageRet :: FilePath -> IO String
    copyImageRet path = readImageRGBA RPU path >>= (\img -> return (preprocessedFilesPath ++ last (splitOn "/" path)) >>= 
        (\output -> writeImage output img >> return output))

    -- | Copies an image to working directory
    copyImage :: FilePath -> IO ()
    copyImage path = readImageRGBA RPU path >>= (\img -> return (preprocessedFilesPath ++ last (splitOn "/" path)) >>= 
        (\output -> writeImage output img))
