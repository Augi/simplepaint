{-# LANGUAGE ScopedTypeVariables #-}
    -- allows pattern signatures like
    -- do
    --     (b :: Behavior Int) <- stepper 0 ...
{-# LANGUAGE RecursiveDo #-}
    -- allows recursive do notation
    -- mdo
    --     ...

{-|
Module      : Main

A desktop application that allows some image transformations described in "Preprocessing.Transformations" on previously chosen file. 
GUI used in the application is a C++ library <http://www.wxwidgets.org/ wxWidgets> which is integrated with haskell thanks to 
<https://wiki.haskell.org/WxHaskell xHaskell> and <https://wiki.haskell.org/Reactive-banana Reactive-banana>. The last one provides
Functional Reactive Programming approach. Due to poor image/bitmap operations in xHaskell, another library called 
<https://hackage.haskell.org/package/hip Haskell Image Processing> was used. Hip is a tool that wraps well known image processing 
libraries: JuciyPixels and Repa.
-}
module Main where
    
    import Graphics.UI.WX hiding (Event)
    import Graphics.UI.WXCore
    import Reactive.Banana
    import Reactive.Banana.WX
    import System.IO.Unsafe

    import Constants
    import FileChooser
    import Preprocessing.Transformations

    -- | Starts the application
    main :: IO ()
    main = start gui

    -- | Main loop of the application that handles both User Interface content and Functional Reactive Programming Network
    gui :: IO ()
    gui = do
        ------------------------------------------- GUI CONFIGURATION -------------------------------------------------------
        -- the main frame
        f <- frame [text := "Simple Paint", resizeable := False, fullRepaintOnResize := False]

        -- variable to hold a path to chosen image
        vdefaultbitmap <- variable [value := Nothing]
        -- variable to hold a path to preprocessed image
        vbitmap <- variable [value := Nothing]

        -- canvas like panel to draw on
        pp <- panel f [bgcolor := white ]

        -- buttons
        defaultButton <- button f [text := "Default"]
        grayscaleButton <- button f [text := "Grayscale"]
        rotateButton <- button f [text := "Rotate"]
        resizeButton <- button f [text := "Resize"]
        sobelButton <- button f [text := "Sobel filter"]
        gaussianButton <- button f [text := "Gaussian filter"]

        -- create file menu
        fileMenu   <- menuPane      [text := "&File"]
        open   <- menuItem fileMenu [text := "&Open\tCtrl+O",  help := "Open an image"]
        menuLine fileMenu
        quit   <- menuQuit fileMenu [help := "Quit program"]

        -- create statusbar field
        status <- statusField   [text := "Welcome to the Simple Paint app!"]
        
        set f [ layout := margin 10 $ column 5 [
                    row 5 [
                        widget defaultButton,
                        widget rotateButton,
                        widget grayscaleButton,
                        widget resizeButton,
                        widget gaussianButton,
                        widget sobelButton
                    ],
                    minsize (sz width height) $ widget pp
                ],
                statusBar := [status],
                menuBar := [fileMenu],
                on (menu quit)   := close f,
                on (menu open)   := onOpen f vdefaultbitmap vbitmap status
            ] 

        -------------------------------------------- FRP NETWORK CONFIGURATION ------------------------------------------------
        let networkDescription :: MomentIO () 
            networkDescription = do
            
            eDefault <- event0 defaultButton command
            eGrayscale <- event0 grayscaleButton command
            eRotate <- event0 rotateButton command
            eResize <- event0 resizeButton command
            eGaussian <- event0 gaussianButton command
            eSobel <- event0 sobelButton command

            let
                preprocess :: String -> Maybe (Bitmap ()) -> Maybe (Bitmap ()) 
                preprocess btnText _ = unsafePerformIO ( do
                    defaultImgPath <- get vdefaultbitmap value
                    imgPath <- get vbitmap value
                    case defaultImgPath of
                        Nothing -> return Nothing -- no image chosen
                        Just defaultPath -> case imgPath of
                            Nothing -> return Nothing -- in case of error, should never hit this line
                            Just path -> ((case btnText of
                                "default" -> copyImage defaultPath
                                "grayscale" -> grayscale path
                                "rotate" -> rotate path
                                "resize" -> resizeBm path width height
                                "sobel" -> sobel path 
                                "gaussian" -> gaussian path
                                ) >> (bitmapCreateFromFile path) >>= (\bm -> return $ Just bm))
                    )

            (bButtonType :: Behavior (Maybe (Bitmap ()))) <- accumB Nothing $  unions
                [
                    (preprocess "default") <$ eDefault,
                    (preprocess "grayscale") <$ eGrayscale,
                    (preprocess "rotate") <$ eRotate,
                    (preprocess "resize") <$ eResize,
                    (preprocess "sobel") <$ eSobel,
                    (preprocess "gaussian") <$ eGaussian
                ]
            
            let
                onPaint :: (Maybe (Bitmap ())) -> DC a -> b -> IO()
                onPaint bitmap dc viewArea = do 
                    case bitmap of
                        Nothing -> return () -- no image chosen
                        Just bm -> drawBitmap dc bm pointZero False []

            sink pp [on paint :== onPaint <$> bButtonType]

            -- inform UI about changes
            eButtonTypeChanged <- changes bButtonType
            reactimate $ repaint pp <$ eButtonTypeChanged

        network <- compile networkDescription    
        actuate network
    