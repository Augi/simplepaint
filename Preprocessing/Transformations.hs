{-|
Module      : Preprocessing.Transformations

Available operations on images in the application.
-}
module Preprocessing.Transformations where

    import Graphics.Image as I

    -- | Reads an image and saves it in grayscale
    grayscale :: FilePath -> IO ()
    grayscale path = readImageYA RPU path >>= (\img -> writeImage path img)
    
    -- | Reads an image and saves it resized
    resizeBm :: FilePath -> Int -> Int -> IO ()
    resizeBm path width height = readImageRGBA RPU path >>= (\img -> writeImage path $ resize Bilinear Edge (width, height) img)
    
    -- | Reads an image and saves it rotated by 90 degree
    rotate :: FilePath -> IO ()
    rotate path = readImageRGBA RPU path >>= (\img -> writeImage path $ rotate90 img)

    -- | Reads an image, applies Gaussian filter and saves
    gaussian :: FilePath -> IO ()
    gaussian path = do
        img <- readImageRGBA RPU path
        writeImage path $ applyFilter (gaussianBlur 1) img

    -- | Reads an image, applies Sobel filter and saves
    sobel :: FilePath -> IO ()
    sobel path = do
        img <- readImageRGBA RPU path
        let imgX = convolve Edge (fromLists [[-1, 0, 1], [-2, 0, 2], [-1, 0, 1]]) img
        let imgY = convolve Edge (fromLists [[-1,-2,-1], [ 0, 0, 0], [ 1, 2, 1]]) img
        writeImage path $ normalize $ sqrt (imgX ^ 2 + imgY ^ 2)
